---
layout: handbook-page-toc
title: Support Modes of Work (and how to do them)
category: Handling tickets
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Modes of Active Support Work

The core responsibility of a Gitlab support engineer is to resolve customer tickets on a daily basis. As part of this core work, Support engineers will update documentation, cut issues to Development teams and submit code fixes as a result of finding and fixing customer problems (and making sure they stay fixed). When a Support engineer has done their fair share of ticket work (we're a team!), there are other Support areas to work on (Hiring, Training, Support Projects etc) and opportunities to contribute to other parts of the company.

We currently have four rotating roles so that people know what to do and have variation in their work:

1. [On call](/handbook/support/on-call/)
1. [First Response Time Hawk](#first-response-time-hawk)
1. [SLA Hawk](#sla-hawk)
1. [Working on Tickets](#working-on-tickets)

## Tips for handling tickets (all roles)

1. **I'm working on a ticket, how do I let the team know I'm working on a reply?**
    1. It's OK to put a ticket **on-hold** while you work on a reply. This takes it out of the main queue so other engineers won't waste time looking at it
    1. If you do this, the SLA clock is still ticking
    1. You must either then send a reply to the customer and submit as Pending
    1. Or if you don't come up with a public reply, summarise your research in an internal note and set the ticket back to Open so another engineer can pick it up. You're encouraged to pair and discuss in Slack to help get a reply out.
1. **A ticket is close to breaching, I'm working on it, but I need more time for my full reply.**
    1. It's OK to send a short reply to the customer to prevent the ticket breaching.  Let them know you're actively working on their ticket and ask if they have any updates since the last reply.
    1. If we do this we **must be good on our promise and get back to them with a full reply**. Sending the short update will stop the SLA clock, so it's up to you to make sure you follow up. **Don't send a short reply to prevent a ticket breaching if you're not planning to follow up yourself.**


## First Response Time Hawk
### What is a First Response Time Hawk?

This is a rotating role, where someone is "on point" weekly (Monday to Friday), to make sure:

1. new Premium SLA tickets get a first response as soon as possible and do not breach
1. all new tickets are appropriately categorized and prioritized as they arrive

First Response Time Hawks directly drive achievement in our [Service Level Agreement KPI](/handbook/support/#service-level-agreement-sla).

### The Process in Action

1. Every Week on Fridays, Managers will add a First Response Hawk for each region to the Support Week in Review to announce who will be responsible that week.
1. If you are FRT Hawk, you are responsible for:
    1. Premium and Ultimate first replies for tickets created from 9am - 5pm in your time zone
       - **Note:** if you are a US Citizen and have access to the Federal instance, this should also be a part of your rotation.
    1. Triaging tickets to make sure they have:
      * An associated organization
      * [The correct priority](/handbook/support/workflows/setting_ticket_priority.html#setting-ticket-priority)
      * The correct ticket form (such as GitLab.com, if they're asking for GitLab.com support). Take special notice of tickets with form `Other Request`. This is usually because the ticket was created by email. **Change the form to the most appropriate form type and then fill in additional metadata where possible**.
      * The correct **Self-managed Problem Type** selected
1. [Keep an eye on the FRTH View in ZenDesk](https://gitlab.zendesk.com/agent/filters/360060245433)
1. You have full authority to call in and ask others for help if volume is high, or you are stumped.
1. You should expect to see new and different things that you are not an expert in. Where possible, take the time to dive deep and try and understand the problem.

### Things to be mindful of

1. Given that there's a FRT Hawk _per timezone_, your shift will overlap with another hawk.
    * This means you have a teammate within a team; yay! Be sure to communicate with your fellow hawk so you can help each other out in case one of you is stuck on a ticket, and also to make sure you're not
    both quietly working on the same ticket

1. Your focus should be new tickets in the Premium & Ultimate queue, especially high and medium priority. If there are no new tickets in that queue, please [associate needs-org tickets with the appropriate organization](associating_needs_org_tickets_with_orgs.html). From there, you can jump to the Starter & Free queue.

1. You might not cover all the tickets you wanted to cover on your shift, and that's okay
   * Just do your best 
   * Take a break if you need one - making sure your team knows you're stepping away by posting in `#support-team-chat`
   * Ask the rest of the team for help when needed in `#support_self-managed`

1. You may see tickets requesting information that is not in the realm of support. For example, you may get a ticket from a job applicant to GitLab requesting information on their application. In these cases, do your best to direct them to an appropriate contact and close the support ticket. In the case of hiring questions, you can ping the #recruiting channel in Slack to inform them that the candidate reached out, and update the candidate via the ticket.
   * There is a macro called `General::Job Application Questions` for hiring-specific questions in Zendesk.

### Dashboards

You can track your progress and see global results on this [Zendesk Explore Dashboard](https://gitlab.zendesk.com/explore/dashboard/36925DBD1F5E3C7BA541DB38D11AC51E0EAAFDD30DCB63FDE83CF1389E555D96/tab/10602202)

## SLA Hawk
### What is the SLA Hawk role?
The SLA Hawk is a rotating role that you will carry out for one week at a time (Monday to Friday). You can [view the rotations in PagerDuty](https://gitlab.pagerduty.com/schedules#?query=sla%20hawk)

The SLA Hawk is responsible for ensuring that action is taken on the 'most breached' and 'next to breach' tickets in order to meet our SLAs and so that difficult tickets are not ignored. **NOTE:** It is the responsibility of all Support team members to ensure customer tickets are responded to within our SLA times. The SLA Hawk is **not expected to reply to tickets themselves - your job is to find team members to help get a reply out (see below for more info)**

The SLA Hawk should also perform a queue tidy up:
* Ensure tickets are in correct queue
* Tickets have the correct Priority
* Tickets have the correct self-managed problem type
* The Customer has a Support Contract
* Solve the ticket if the support request is solved

SLA Hawks drive achievement via our KPI's for [Service Level Agreement KPI](/handbook/support/performance-indicators/#service-level-agreement-sla) and [Customer Satisfaction](/handbook/support/performance-indicators/#support-satisfaction-ssat) by making sure our customers are updated in a timely manner.

### The Process in Action

1. Each day, sort the Zendesk view for [Self Managed Premium and Ultimate](https://gitlab.zendesk.com/agent/filters/304899127) by '*Next SLA Breach*' ascending, starting with most breached ticket first.
2. Read the ticket yourself and if you feel you know what the next action is, do that (e.g. ask the customer for more information, solve the ticket if the issue is resolved, send your own reply if you have a great idea). There's no need to spend too long at this stage - 10 minutes at most. If the next action requires more work move on to the next step.
3. If you feel the priority does not match our [Definitions of Support Impact](/support/#definitions-of-support-impact) reach out to the customer and agree on the new priority. Use the macro [General::Changed priority](https://gitlab.zendesk.com/agent/admin/macros/360093631494).
4. If a ticket **has an assignee**:
   1. If the assignee is in your Region, link the ticket in `#support-self-managed` and ping them asking if they are able to take a look. We've adopted a convention of prefixing your message in Slack with `[SLAH]` so that folks know you're currently in that role.
   2. If the assignee is from another Region and its during their “office hours” ask them if they could take a look. 
   3. If the assignee is offline and the ticket/customer needs an update, follow the **has no assignee** process below.
   4. If assignee can’t currently work the ticket ask them to follow the Process in action for [Working on Tickets](#the-process-in-action-2).
5. If a ticket **has no assignee**:
   1. Check to see if an engineer from your region has previously responded on the ticket, if so, ask them would they have the bandwidth to take the ticket.
   2. If you can't locate a specific person, let the team know (e.g. `@support-team-emea` ) about the ticket. Once you have one or two volunteers, let the team know the ticket is taken care of via the thread.
   3. If you’re not getting any traction, and you feel you need help, ping your manager to let them know that noone is available to help.
   4. Once someone has taken ownership of the ticket, ask them if they're happy to assign it to themselves.
6. If there are no imminent breaches (e.g. less than 2 to 4 hours) in the 'Self Managed Premium and Ultimate' queue - repeat the process with the [Starter and Free](https://gitlab.zendesk.com/agent/filters/360062019453) view. 

### Team Responsibilities for the SLA Hawk
* Help the SLA Hawk! If they ask you for help, please acknowledge (You may not be able to help, just let them know). 
* Follow the [Support Modes of Work](#the-process-in-action-2) for the tickets in your own queue (including finding another engineer to help or takeover the ticket).
* When looking for your next ticket, follow the [same process](#the-process-in-action-1) as the SLA Hawk above (most breached ticket first).
* If you take a next response ticket and the ticket doesn’t have an assignee, assign yourself to the ticket.

## FAQ about SLA Hawk role

1. **Do I need to do the SLA Hawk role all day every day?** The role is not to work the tickets yourself but rather find an engineer to respond to the ticket. You probably only need to check for breached and breaching tickets 3 or 4 times per day, which fits in with our asynchronous working environment while ensuring that synchronous customer interaction is still maintained. This should make the role light-weight. Between ticket checkins, you can continue with your planned day.
1. **#I feel uncomfortable asking someone else to reply to a ticket** We recommend prefixing your message in Slack with `[SLAH]`, this lets people know that you've been given the role to find people to help. We assume positive intent and the team knows that this is the job of the SLA Hawk.

### Dashboards

You can see how we're doing on [this Zendesk Explore Dashboard](https://gitlab.zendesk.com/explore/dashboard/36925DBD1F5E3C7BA541DB38D11AC51E0EAAFDD30DCB63FDE83CF1389E555D96/tab/10602202).

Look a the 'Self Managed NRT SLA achieved charts' for this week and last week. We're aiming for 85% achievement. If we can get to 95% that would be awesome! The 'this week' chart updates hourly.

## Working on Tickets
### What is the "Working on Tickets" workflow?
If you don't have a specialized role this week, you'll be working the "Working on Tickets" workflow. The "Working on Tickets" workflow is the core responsibility of a Gitlab Support Engineer, and it is all about resolving the problems that customers present to us by submitting support tickets. The focus is **being responsible for the continuous progress toward resolution of a specific set of tickets**. What does this look like?

It starts with assigning a ticket to yourself at the moment when you make the first public comment on it. From that point forward you should be thinking about how you can keep that ticket moving toward a resolution that's acceptable to the customer. Don't worry - you're not responsible for creating the solution, only for making the solution happen. **You're the leader, not the sole contributor.** If you can resolve the ticket without help, great! If you can't, ask others to help by working with you (e.g. create a thread in Slack or arrange a pairing session). If you're not able to find someone to work with you please let your manager know so that they can help with next steps.

Benefits of working on tickets assigned to yourself:
1.  You won't have to read through a series of updates from multiple support engineers, and the customer, to understand the ticket history, current status, or plan of action. In fact, we encourage you to include a brief description of those items - history (what's happened so far), current status, next steps - in most of your updates to the customer so that your last update always tells you what you need to know.
1.  With one person in charge of each ticket, you can take a consistent approach to resolving the problem, you can explain why and what you're doing, and you won't repeat or contradict an action taken by somebody else on the ticket. This is a much better experience for the customer.
1.  Knowing that you're in charge of a ticket gives you freedom to set and manage customer expectations at each step along the way, so the customer will know what's going on and who's working on their behalf. Again, this is a customer experience enhancement.

When you're "Working on Tickets", you're driving achievement of our KPI of [Support Satisfaction](/handbook/support/#support-satisfaction-ssat) by helping to resolve tickets as quickly and effectively as possible.

### The Process in Action
Here's what to do when you're actively working on tickets in Zendesk. You will be dividing your efforts between:

*  taking **next** steps on tickets that are already assigned to you and so can be found in your own queue (the `My assigned tickets` view), and
*  identifying and taking **first** steps on new tickets in the main queue (`Self-Managed MY_REGION & All Regions - Needs assignee` view).

It looks like this:
1. Start your work at the top of your own queue
1. Determine whether you can make progress and take the next steps
   1. If you can, then take those steps, including updating the customer
   1. If not, then link the ticket in Slack and ask for help from your teammates
1. `Finding a New Ticket` If it's been an hour or more since you checked the main queue (or your own queue is empty), and you have the capacity to take on another ticket, then:
   1. If the ticket at the top of the main queue has an SLA that will expire in an hour or less, assign that ticket to yourself 
1. If you've asked in Slack for help on a ticket, and nobody has stepped up to help, and you feel you can't wait any longer for help, then:
   1. `@mention` your manager in Slack in the thread where you requested help, and ask them what the next steps should be
1. If there are tickets left in your view to work on, then go back to step 1
1. Go back to `Finding a New Ticket`

See the [Working on Tickets Flowchart](#working-on-tickets-flowchart) for a visual representation.

<!-- ![Working on Tickets process flowchart](assets/working-on-tickets-workflow.png) -->

### Keep In Mind:

1. Teamwork is really important with this workflow. Be aware of what's happening across the board and jump in! Pairing with someone is a great way to work out complex issues.
1. If you respond to a ticket and set it to `Open` or `On-hold`, ZenDesk will remove the SLA and automatically assign it to you (if it's not already assigned to someone else).
1. For all tickets in which the user is waiting on a response from Support, always provide an update on its status and work that has been done. Aim to do this daily, and definitely no less than every 3 days (the length of the On-hold period).

### FAQ About Working on Tickets workflow

1. **How many tickets should I have assigned to me?** There's no specific number. Aim to take as many tickets as you can while being sure that you can give good attention to each of them daily (unless they’re on hold). You might expect that to be somewhere between three and ten open, pending or on-hold tickets in your [My Assigned Tickets](https://gitlab.zendesk.com/agent/filters/360062369834) view.
1. **A ticket is assigned to someone else - can I work on it?** Absolutely! Support Engineers should balance their day between being 'proactive' (working on your assigned tickets) and 'reactive' (helping prevent tickets from breaching their SLAs). Tickets that are soon to breach will often be assigned to a team member in a different region. When replying to these tickets, be sure that your next steps align with the action plan that the assignee has described on their replies or ticket summary.
1. **I'm going to be absent, what do I do with my assigned tickets?** If you'll be absent for only a day or two, please ask your customers whether they want to wait for your return or work with others in your absence. For those who will wait, place their tickets on hold. For longer absences, please inform your customers that your tickets will be reassigned due to your upcoming absence. Then, find someone willing to take over each ticket, and review the tickets with them before reassigning. If you can't find someone to assign your ticket to, you can 'unassign' by assigning it to a 'group' (e.g. 'Support EMEA', 'Support APAC', 'Support AMER'). It doesn't matter which group - the ticket will still be visible for all Support Engineers. Be sure in this situation to create a very clear and thorough ticket summary so that the next engineer can come up to speed very quickly.
1. **A ticket has diverged into more than one problem. What do I do?** It's recommended to keep tickets focused on a single problem that's clearly described by the ticket Title. If the customer asks about another problem, you are encouraged to create a new ticket on behalf the customer that focuses on a single issue and keep the original ticket on the original issue. This helps reduce time to resolution and makes it easier for us to focus on fixing the problem at hand.
1. **Can I reassign a ticket to someone else?** There are some situations in which you should look to reassign a ticket:
   1. If you'll be absent, see the FAQ above about reassigning tickets when you'll be absent.
   1. If you've determined that specific expertise outside your own is required to resolve the ticket, your first choice should be to pair with an expert so that you can get the ticket resolved and learn in the process. But if pairing is not reasonable, go ahead and reassign the ticket after discussing it with the other support engineer.  Be sure to send a message to the customer informing them that you’ve asked another support engineer with relevant expertise to take over the ticket, and that you’ve reviewed the ticket with that engineer.
   1. If you've become overloaded with tickets, feel free to rebalance your load by finding one or more teammates to take over some of your tickets. Be sure you discuss each ticket before reassigning it so that the other support engineers don't have to start from scratch. Then inform the customer that you’ve asked another support engineer to take over the ticket due to time constraints.

## Mermaid Source

#### SLA Hawk

~~~

   ```mermaid

   graph TD
   A[Sort 'Premium and Ultimate view by <br> 'Next SLA Breach'] -->
   B(Open the <br> top ticket)
   B --> C{Can you reply <br> to the ticket?}
   C -->|Yes| D[Send <br> reply] --> H
   C -->|No| E[Ask for help in Slack <br> and link the ticket]
   E -->|Help received| F[Send <br> reply] --> H
   E -->|No help recieved| G[Mention your manager in Slack <br> on the ticket thread] --> H[Go to the next ticket <br> - if no imminent breaches - <br> go to 'Starter and Free' view] --> B
   ```

~~~

## Working on Tickets Flowchart

```mermaid
graph TD
A([My Tickets]) --> B
B[Open the 'My Assigned Tickets View' in Zendesk] --> B1
B1{Are there tickets left in <BR> your view that need work?} -->|Yes| C
B1 -->|No| NT2
C[Select top ticket from queue] --> D
D{Can you make progress and <BR> take the next steps?}
D -->|Yes| E[Update the customer<BR>with the next steps]
D -->|No| F[Link the ticket in<BR>Slack and ask <BR> teammates for help]
E & F --> NT0

NT0([Finding a New Ticket]) --> NT1
NT1{Have you checked <BR> the main queue <BR> in the last hour?} -->|Yes| AH0
NT1 -->|No| NT2
NT2{Do you have capacity <BR> for another ticket?} -->|No| AH0
NT2 -->|Yes| NT3
NT3{Is there a ticket in the <BR> main queue expiring <BR> within an hour?} -->|No| AH0
NT3 -->|Yes| NT4
NT4([Assign the ticket to yourself]) --> B1

AH0([Check on Help Requests]) --> AH1
AH1{Is there a ticket <BR> on which you need <BR> help but nobody's <BR> volunteered?} -->|Yes| AH2
AH1 -->|No| B1
AH2["@"mention your manager in <BR> Slack in the thread <BR> where you requested help. <BR> Ask them for next steps.] --> B1
```
