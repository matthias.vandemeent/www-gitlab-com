---
layout: markdown_page
title: "SCM Message House"
---
 


| **Positioning Statement** | Application development and DevOps is fast and iterative, making it essential that teams can rapidly collaborate and iterate on new features and deliver business value. They must avoid working in silos which creates complex integration conflicts and constantly broken builds. GitLab is a comprehensive Source Code Management (SCM) solution to deliver better software faster. A Git-based web repository that aggregates all development milestones and metadata, GitLab enables clear code reviews, asset version control, feedback loops, and powerful branching patterns to streamline software delivery.  GitLab helps you deliver faster, more efficiently, and increase compliance.|
|------------------------|-------------------------------------------------------------------------|
| **Short Description** | GitLab makes sense of SCM by a seamless interface to collaborate and coordinate work, review changes, and manage delivery so team members focus on solving problems and shipping value |
| **Long Description** | Source Code Management coordinates all changes in a software project, effectively tracking changes to source files, designs, and all digital assets required for the project as well as related metadata.  Projects without SCM can easily devolve into an uncontrollable mess of different versions of tonnes of project files, hindering the ability of any software development team to deliver value. GitLab makes SCM easy assembling all critical project files and assets in one seamless interface and delivering it through a single application for the entire software development lifecycle.   |


| **Key-Values** | *VISIBILITY*  | *COLLABORATION* | *VELOCITY* |
|--------------|------------------------------------------------------------------|----------|----------|
| **Promise** | Visibility and traceability across projects, groups, epics and issues. Gain insight from aggregate dev metrics and assess gaps and bottlenecks. Analyse in detail each work flow and monitor task and feature progress | Effective, contextual and actionable communication to improve code quality. Set permissions, code owners and process approvers. Keep compliance frictionless and enable team members to push progress one commit at a time | Lightning-speed branching, automation rules for Merge Requests and CI  |
| **Pain points** | Development teams require a specific project file structure often dictated by the technology, architecture, domain, and goals of the project. Team leads and dev managers struggle to organize projects and teams to meet the company and leadership's expectations the most efficient way. It is not easy to connect the dots | It is difficult to collaborate and share feedback in the appropriate context and make it actionable immediately. At the same time, projects and groups require different levels of access and privileges which in turn result in a complex maze of permissions | Projects can become heavy and bloated over time with tens of thousands of files, versions, and associated metadata history, effectively making future changes and branching complex, error-prone and expensive. Lightweight projects, on the other hand, can be over-engineered easily |
| **Why GitLab** | GitLab Insights and Compliance Dashboard can aggregate and compose dashboards for Team Leads and Product Managers to capture bottlenecks, apply solutions and foster continuous improvement |  GitLab's Merge Requests and Code Quality provide the perfect canvas to manage all aspects of changing code and project assets, enabling reviews, feedback, and immediate action to improve code  | GitLab Merge Request design, automated merge approvals, issue closing, and more automate tedious tasks and allow developers to focus on shipping features|


| Proof points | [Paessler's 90% of QA is self served](https://about.gitlab.com/customers/paessler/) thanks to immediate, contextual feedback
| | [European Space Agency's](https://about.gitlab.com/customers/european-space-agency/) different units have gained visibility across projects: code and application reuse happen across teams |
